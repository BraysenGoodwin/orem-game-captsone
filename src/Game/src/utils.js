export function randomColor() {
  let dec = Math.floor(Math.random() * 16777215)
  return "#" + dec.toString(16)
}
