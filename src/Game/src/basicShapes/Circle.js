import Entity from '../Entity'

export default class Circle extends Entity {
  constructor(x, y, r, fillColor="blue", outlineColor="transparent") {
    super()
    this.x = x
    this.y = y
    this.r = r
    this.outlineColor = outlineColor
    this.fillColor = fillColor
  }

  render(context) {
    context.beginPath()
    context.strokeStyle = this.outlineColor
    context.arc(this.x, this.y, this.r, 0, Math.PI * 2)
    context.stroke()
    context.fillStyle = this.fillColor
    context.fill()
  }
}
