import Entity from '../Entity'

export default class GameImage extends Entity {
    constructor(x,y,width,height,url) {
        super()
        this.x = x
        this.y = y
        this.width = width
        this.height = height;
        this.image = new Image(this.width, this.height);
        this.image.src = url;
    } 
    
    render(context) {
        context.beginPath()
        context.drawImage(this.image, this.x, this.y, this.width, this.height)
    }
}