import Entity from '../Entity'

export default class Rectangle extends Entity {
    
    constructor(x,y,width,height) {
        super()
        this.x = x
        this.y = y
        this.width = width
        this.height = height
    }
    
    render(content) {
        content.beginPath()
        content.rect(this.x,this.y,this.width,this.height);
        content.stroke();
    }
}