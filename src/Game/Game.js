import React, { Component } from 'react';

import Snow from './src/example/Snow'
import Rectangle from './src/basicShapes/Rectangle'
import GameImage from './src/basicShapes/Image'
import { randomColor } from './src/utils'



export default class Game {

  constructor() {
    this._context = {
      canvas: null,
      context: null,
    }
    this.interval = null
    this.loopSpeed = 30
    this.alreadyInitialized = false
  }

  initialize(canvas, callback) {
    console.log('game initializing')
    if (this.alreadyInitialized) {
      callback()
    }
    this.canvas = canvas
    //full screen
    // canvas.height = window.innerHeight;
    // canvas.width = window.innerWidth;


    //sive canvas to fit window
    canvas.style.width ='100%'
    canvas.style.height='100%'

    canvas.width  = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;


    this.alreadyInitialized = true
    callback()
  }

  set canvas(canvas) {
    this._context.canvas = canvas;
    this._context.context = canvas.getContext('2d');
  }

  load = () => {
    /*load resources*/
  }//

  start = () => {
    console.log("game started")
    /*start the game*/
    const {context} = this._context
    // this.rect = new Rectangle(50, 50, 20, 20)
    const size = 0;
         //drag drop place tower items
  
 
   

    const imgs1 = ["/assets/tower-imgs/lazer-tower1.png", "/assets/tower-imgs/cannon-tower1.png", "/assets/tower-imgs/sniper-tower1.png", "/assets/tower-imgs/lightning-tower1.png", "/assets/tower-imgs/hyper-tower1.png"]
    this.imgs = []
var i;
for (i = 0; i < 5; i++) { 
  const pic = imgs1[i];
  const build = this.imgs.push(new GameImage(5 , 5+(50*i) , 50 , 50 , pic));
  

   {build}
    
  

}
  

     
     
    // this.snow = []

    // for (let i = 0; i < 1000; i++) {
    //   this.snow.push(new Snow(Math.random() * this._context.canvas.clientWidth, Math.random() * this._context.canvas.clientHeight, Math.random() * 4 + 1, Math.random() * 10, Math.random() * 4 + 1, randomColor()))
    // }

    this.interval = setInterval(this.gameLoop, this.loopSpeed)
  }

  gameLoop = () => {
    this.tick()
    this.render()
  }

  pause = () => {
    console.log("game paused")
    /*pause the game*/
    if (this.interval) {
      clearInterval(this.interval)
    }
  }

  resume = () => {
    console.log("game resumed")
    /*pause the game*/
    this.interval = setInterval(this.gameLoop, this.loopSpeed)
  }

  tick = () => {
    /*handle tick and updates*/
    const {context} = this._context
    // for (let snow of this.snow) {
    //   snow.tick(context)
    // }
  }

  render = () => {
    /*handle render*/
    const {context} = this._context
    
    this.imgs.forEach(img => img.render(context))
    
    // this.rect.render(context)
  // this.img.render(context)
    // context.clearRect(0,0, this._context.canvas.clientWidth, this._context.canvas.clientHeight)
    // for (let snow of this.snow) {
    //   snow.render(context)
    // }
  }

  get context() {
    return this._context.context
  }
}
//made by Joshua Jardine