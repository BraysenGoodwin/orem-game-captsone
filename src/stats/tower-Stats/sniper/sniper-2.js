const sniperTower2 = sniperTower2;
    
    sniperTower2.dmg = 14;
    sniperTower2.rof = 1;
    sniperTower2.size = 2;
    sniperTower2.range = 100;

    sniperTower2.upgradeCost = 100;
    
    sniperTower2.upgradeText = "shrapnel shot";


export default sniperTower2