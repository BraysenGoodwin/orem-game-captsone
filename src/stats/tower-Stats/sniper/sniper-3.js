const sniperTower3 = sniperTower3;
    
    sniperTower3.dmg = 12;
    sniperTower3.rof = 1;
    sniperTower3.size = 2;
    sniperTower3.range = 100;

    sniperTower3.upgradeCost = 250;
    
    sniperTower3.impact = 3;
    
    
    sniperTower3.upgradeText = "semi-Automatic";


export default sniperTower3