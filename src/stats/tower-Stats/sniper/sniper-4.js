const sniperTower4 = sniperTower4;
    
    sniperTower4.dmg = 12;
    sniperTower4.rof = 3;
    sniperTower4.size = 2;
    sniperTower4.range = 100;

    sniperTower4.upgradeCost = 300;
    
    sniperTower4.impact = 1;
    
    
    sniperTower4.upgradeText = "Supply depot";


export default sniperTower4;