const sniperTower1 = sniperTower1;
    
    sniperTower1.dmg = 10;
    sniperTower1.rof = 1;
    sniperTower1.size = 2;
    sniperTower1.range = 100;
    sniperTower1.initCost = 80;
    sniperTower1.upgradeCost = 5;
    
    sniperTower1.upgradeText = "point five oh";


export default sniperTower1;