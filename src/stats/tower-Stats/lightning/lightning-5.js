const lightningTower5 = lightningTower5;
    
    lightningTower5.dmg = 10;
    lightningTower5.rof = 2;
    lightningTower5.size = 3;
    lightningTower5.range = 8;

    lightningTower5.upgradeCost = 5;
    
    lightningTower5.impact = 3;
    
    lightningTower5.upgradeText = "MAX LEVEL";


export default lightningTower5