const lightningTower4 = lightningTower4;
    
    lightningTower4.dmg = 8;
    lightningTower4.rof = 1.5;
    lightningTower4.size = 2;
    lightningTower4.range = 7;

    lightningTower4.upgradeCost = 5;
    
        lightningTower4.impact = 2;
    
    lightningTower4.upgradeText = "upgrade to storm gun";


export default lightningTower4