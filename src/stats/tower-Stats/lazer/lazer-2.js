const lazerTower2 = lazerTower2;
    
    lazerTower2.dmg = 5;
    lazerTower2.rof = 3;
    lazerTower2.size = 1;
    lazerTower2.range = 8;
    lazerTower2.upgradeCost = 90;

    lazerTower2.upgradeText = "Double ROF";


export default lazerTower2;