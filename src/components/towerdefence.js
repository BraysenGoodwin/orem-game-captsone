import React, { Component } from 'react';
import Game from './Game';
    // import lazerTower1 from '../stats/tower-Stats/lazer-1.js';
    // import cannonTower1 from '../stats/tower-Stats/cannon-1.js';
    // import lightningTower1 from '../stats/tower-Stats/lightning-1.js';
    // import sniperTower1 from '../stats/tower-Stats/sniper-1.js';
    // import hyperTower1 from '../stats/tower-Stats/hyper-1.js';
    
    // import lazerTower2 from '../stats/tower-Stats/lazer-2.js';
    // import cannonTower2 from '../stats/tower-Stats/cannon-2.js';
    // import lightningTower2 from '../stats/tower-Stats/lightning-2.js';
    // import sniperTower2 from '../stats/tower-Stats/sniper-2.js';
    // import hyperTower2 from '../stats/tower-Stats/hyper-2.js';
    
    // import lazerTower3 from '../stats/tower-Stats/lazer-3.js';
    // import cannonTower3 from '../stats/tower-Stats/cannon-3.js';
    // import lightningTower3 from '../stats/tower-Stats/lightning-3.js';
    // import sniperTower3 from '../stats/tower-Stats/sniper-3.js';
    // import hyperTower3 from '../stats/tower-Stats/hyper-3.js';
    
    // import lazerTower4 from '../stats/tower-Stats/lazer-4.js';
    // import cannonTower4 from '../stats/tower-Stats/cannon-4.js';
    // import lightningTower4 from '../stats/tower-Stats/lightning-4.js';
    // import sniperTower4 from '../stats/tower-Stats/sniper-4.js';
    // import hyperTower4 from '../stats/tower-Stats/hyper-4.js';
    
    // import lazerTower5 from '../stats/tower-Stats/lazer-5.js';
    // import cannonTower5 from '../stats/tower-Stats/cannon-5.js';
    // import lightningTower5 from '../stats/tower-Stats/lightning-5.js';
    // import sniperTower5 from '../stats/tower-Stats/sniper-5.js';
    // import hyperTower5 from '../stats/tower-Stats/hyper-5.js';
    
        // function text(costText){
        //   const newDiv = document.createElement('div');
        //   newDiv.classList.add('cost-text');
        //   console.log('on')}
        
        var selected = 0
        
        function lazerTower(){
          console.log('off')
          selected = "lazer"
        }
        
        function cannonTower(){
          console.log('off')
          selected = 'cannon'
        }
        
        function sniperTower(){
          console.log('off')
          selected = 'sniper'
        }
        
        function lightningTower(){
          console.log('off')
          selected = 'lightning'
        }
        
        function hyperTower(){
          console.log(selected)
          selected = 'hyper'
          console.log(selected)
        }
        
        function check(){
          if(selected != 0){
            if(selected == 'lazer'){
              
              selected = 0
            }else if(selected == 'cannon'){
              
              selected = 0
            }else if(selected == 'sniper'){
              
              selected = 0
            }else if(selected == 'lightning'){
            
              selected = 0
            }else if(selected == 'hyper'){
              
              selected = 0
            }
          }else {
            console.log("space selected")
          }
        }

export default class TowerDefence extends Component {
  render() {
    return (
      <div className='grid-wrapper'>
        <h1 className='title'>2018 Capstone Game</h1>
        
        <div className='game-Picker'>
       <a href="/">Home</a>
        </div>
        
        <div className= 'game-box'>
        <div className='lazerTower1'></div>
        </div>
        
        <div className="grid-margin"/>
        
        
  

        
        
      {//all code below here is the html grid over the canvas.  for the sake of time I am just making it as easy as possibe.
      }
        
        
        <div className="gameGrid">

        <div onClick={lazerTower}  className="lazerBuild">LazerTower 100</div>
        <div onClick={cannonTower}  className="cannonBuild">CannonTower 150</div>
        <div onClick={sniperTower} className="sniperBuild">SniperTower 80</div>
        <div onClick={lightningTower} className="lightningBuild">LightningTower 100</div>
        <div onClick={hyperTower} className="hyperBuild">HyperTower 150</div>
        
        <div onClick={check} className="map__4-1"></div>
        <div onClick={check} className="map__4-2"></div>
        <div onClick={check} className="map__4-3"></div>
        <div onClick={check} className="map__4-4"></div>
        <div onClick={check} className="map__4-5"></div>
        <div onClick={check} className="map__4-6"></div>
        <div onClick={check} className="map__4-7"></div>
        <div onClick={check} className="map__4-8"></div>
        
        <div onClick={check} className="map__5-1"></div>
        <div onClick={check} className="map__5-2"></div>
        <div onClick={check} className="map__5-3"></div>
        <div onClick={check} className="map__5-4"></div>
        <div onClick={check} className="map__4-5"></div>
        <div onClick={check} className="map__5-6"></div>
        <div onClick={check} className="map__5-7"></div>
        <div onClick={check} className="map__5-8"></div>
        
        <div onClick={check} className="map__6-1"></div>
        <div onClick={check} className="map__6-2"></div>
        <div onClick={check} className="map__6-3"></div>
        <div onClick={check} className="map__6-4"></div>
        <div onClick={check} className="map__6-5"></div>
        <div onClick={check} className="map__6-6"></div>
        <div onClick={check} className="map__6-7"></div>
        <div onClick={check} className="map__6-8"></div>
        
        <div onClick={check} className="map__7-1"></div>
        <div onClick={check} className="map__7-2"></div>
        <div onClick={check} className="map__7-3"></div>
        <div onClick={check} className="map__7-4"></div>
        <div onClick={check} className="map__7-5"></div>
        <div onClick={check} className="map__7-6"></div>
        <div onClick={check} className="map__7-7"></div>
        <div onClick={check} className="map__7-8"></div>
        
        <div onClick={check} className="map__8-1"></div>
        <div onClick={check} className="map__8-2"></div>
        <div onClick={check} className="map__8-3"></div>
        <div onClick={check} className="map__8-4"></div>
        <div onClick={check} className="map__8-5"></div>
        <div onClick={check} className="map__8-6"></div>
        <div onClick={check} className="map__8-7"></div>
        <div onClick={check} className="map__8-8"></div>
        
        <div onClick={check} className="map__9-1"></div>
        <div onClick={check} className="map__9-2"></div>
        <div onClick={check} className="map__9-3"></div>
        <div onClick={check} className="map__9-4"></div>
        <div onClick={check} className="map__9-5"></div>
        <div onClick={check} className="map__9-6"></div>
        <div onClick={check} className="map__9-7"></div>
        <div onClick={check} className="map__9-8"></div>
        
        <div onClick={check} className="map__10-1"></div>
        <div onClick={check} className="map__10-2"></div>
        <div onClick={check} className="map__10-3"></div>
        <div onClick={check} className="map__10-4"></div>
        <div onClick={check} className="map__10-5"></div>
        <div onClick={check} className="map__10-6"></div>
        <div onClick={check} className="map__10-7"></div>
        <div onClick={check} className="map__10-8"></div>
        
        <div onClick={check} className="map__11-1"></div>
        <div onClick={check} className="map__11-2"></div>
        <div onClick={check} className="map__11-3"></div>
        <div onClick={check} className="map__11-4"></div>
        <div onClick={check} className="map__11-5"></div>
        <div onClick={check} className="map__11-6"></div>
        <div onClick={check} className="map__11-7"></div>
        <div onClick={check} className="map__11-8"></div>
        
        <div onClick={check} className="map__12-1"></div>
        <div onClick={check} className="map__12-2"></div>
        <div onClick={check} className="map__12-3"></div>
        <div onClick={check} className="map__12-4"></div>
        <div onClick={check} className="map__12-5"></div>
        <div onClick={check} className="map__12-6"></div>
        <div onClick={check} className="map__12-7"></div>
        <div onClick={check} className="map__12-8"></div>
        
        <div onClick={check} className="map__13-1"></div>
        <div onClick={check} className="map__13-2"></div>
        <div onClick={check} className="map__13-3"></div>
        <div onClick={check} className="map__13-4"></div>
        <div onClick={check} className="map__13-5"></div>
        <div onClick={check} className="map__13-6"></div>
        <div onClick={check} className="map__13-7"></div>
        <div onClick={check} className="map__13-8"></div>
        

        
          <div className="game-wrapper"><Game className="game-box"/></div>
        </div>
    
        
        

        

      </div>
    );
  }
}
//made by Joshua


