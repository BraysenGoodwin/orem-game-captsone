import React, { Component } from 'react';
import GameDescription from './gamedescription';


export default class App extends Component {
  render() {

    const gameTitles = ["Tower Defense", "Slime Ball", "Star Wars Jedi Man"]
    const gameLinks = ['towerdefence','' ,'' ,'']
    const gameDiscrips = ['This is the 2018 capstone tower defence game.  In it you will construct towers to fight off the alien hordes that threaten humanity.','Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ut, ad omnis. Quo unde voluptatibus voluptate, doloribus distinctio quia voluptas dignissimos iste sint veritatis accusantium eaque dolore ipsa omnis animi magni. ','Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ut, ad omnis. Quo unde voluptatibus voluptate, doloribus distinctio quia voluptas dignissimos iste sint veritatis accusantium eaque dolore ipsa omnis animi magni. ','Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ut, ad omnis. Quo unde voluptatibus voluptate, doloribus distinctio quia voluptas dignissimos iste sint veritatis accusantium eaque dolore ipsa omnis animi magni. ']
    const gameimgs = ["/assets/tower-imgs/lazer-tower1.png", "http://via.placeholder.com/95x62", "http://via.placeholder.com/95x62"]
    
    const gameDescriptions = [];
    for (let i = 0; i < gameTitles.length; i++) {
      const gameTitle = gameTitles[i];
      const gameLink = gameLinks[i];
      const gameDisc = gameDiscrips[i];
      const img = gameimgs[i];
      gameDescriptions.push(<li><GameDescription title={gameTitle} link={gameLink} disc={gameDisc} img={img}/></li>);
    }

    return (
      <div className='grid-wrapper'>
        <h1 className='title'>2018 Capstone Game Website</h1>

        


      <div className='new-games-container'> 
        <h2>NEW GAMES</h2>
        <ul>
          {gameDescriptions}
        </ul>
      </div>
      
      </div>
    );
  }
}
//made by joshua,