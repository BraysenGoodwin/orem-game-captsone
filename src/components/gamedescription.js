import React, { Component } from 'react';

export default class GameDescription extends Component {
  render () {
    return (
      <div className="game-description">
        <img src={this.props.img}></img>
        <div>
          <a href={this.props.link}><h3>{this.props.title}</h3></a>
          <p> {this.props.disc} </p>
        </div>
      </div>
    );
  }
}
// made by ..., and Joshua