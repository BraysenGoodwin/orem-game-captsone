import React, { Component } from 'react'
import Game from '../Game'

export default class GameCanvas extends Component {

  constructor(props) {
    super(props)
    this.state = {
      unupdatingRefs: {
        canvas: null,
      }
    }
  }

  componentWillMount = () => {
    if (!global.game) {
      global.game = new Game()
    }
  }

  componentDidMount = () => {
    global.game.initialize(this.state.unupdatingRefs.canvas, () => {global.game.start()})
  }

  render() {
    let canvas
    return (
      <canvas className={this.props.className} ref={canvas => {this.state.unupdatingRefs.canvas = canvas}} width="640" height="480"></canvas>
    )
  }
}
